# Write codeplug to radio

Use option -w to write a binary codeplug image to the radio.

```
$ dmrconfig -w md-uv380-factory.img
Connect to TYT MD-UV380.
Read codeplug from file 'md-uv380-factory.img'.
Last Programmed Date: 2018-07-13 16:02:59
CPS Software Version: V01.07
Write device: ########################################### done.
Close device.
```

Input binary file can have IMG format (compatible with md380tools)
or RDT format (compatible with native CPS software).
