# Configure the radio with a text script

A configuration script can be applied to the radio with -c option.

The script can contain a complete configuration or any part of it.
Only parts present in the script are replaced with new data,
other parts are preserved. For example, I use the following script
to set DMR ID and name on my radios, without changing channels
and other stuff:

```
$ cat kk6abq.conf
ID: 3114542
Name: KK6ABQ
Intro Line 1: KK6ABQ
Intro Line 2: Sergey
```

Previous state is saved to file backup.img.
It can be restored back using -w option, in case you are not happy with
the result of your script.

```
$ dmrconfig -c kk6abq.conf
Connect to TYT MD-UV380.
Read device: ########################## done.
Last Programmed Date: 2018-08-31 23:23:13
CPS Software Version: V=0.01
Write codeplug to file 'backup.img'.
Read configuration from file 'kk6abq.conf'.
Total 239 channels, 39 zones, 39 scanlists, 35 contacts, 2 grouplists.
Write device: ########################################### done.
Close device.
```
