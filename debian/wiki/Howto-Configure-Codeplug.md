# Configure a codeplug with a text script

A configuration script can be applied to a binary codeplug image with -c option.

The script can contain a complete configuration or any part of it.
Only parts present in the script are replaced with new data,
other parts are preserved. For example, to add a few useful text messages
to any codeplug, the following script can be used:

```
$ cat messages.conf
Message Text
    1   QSY Norcal 95150, pls
    2   QSY Local, pls
    3   QSY California 3106, pls
    4   QSY 441.000 CC1 TS1 TG99 Direct, pls
    5   Be Right Back
    6   My email is mycall@arrl.net
    7   73 . .
```

The source codeplug is not touched.
Result is saved to a new file 'device.img'.

```
$ dmrconfig -c md380-norcal-brandmeister.rdt messages.conf
Read codeplug from file 'md380-norcal-brandmeister.rdt'.
Last Programmed Date: 2016-10-20 17:49:09
CPS Software Version: V01.32
Read configuration from file 'messages.conf'.
Total 239 channels, 39 zones, 39 scanlists, 35 contacts, 2 grouplists.
Write codeplug to file 'device.img'.
```
