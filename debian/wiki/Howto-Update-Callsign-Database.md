# Update Callsign Database

Radios AT-D868UV, AT-D878UV, MD-2017 and MD-UV380 are able to store a full database of DMR IDs and callsigns.
A firmware of your radio should be [upgraded](https://www.buytwowayradios.com/blog/2017/07/how_to_update_the_tyt_md-2017_to_store_100000_contacts.html)
to enable this feature.

Full list of contacts can be obtained from [RadioId.net](https://www.radioid.net/) website as a text file in CSV format. For example:
```
$ wget https://www.radioid.net/static/user.csv
```

With dmrconfig, the CSV file can be uploaded to the radio with the following command:
```
$ dmrconfig -u user.csv 
Connect to TYT MD-UV380.
Read file 'users.csv'.
Total 109122 contacts.
Erase contacts: ########################## done.
Write contacts: ########################## done.
Close device.
```

Resulting view of Contacts CSV List:

![md-uv380-csv-view.jpg](https://raw.githubusercontent.com/wiki/sergev/dmrconfig/files/md-uv380-csv-view.jpg)
