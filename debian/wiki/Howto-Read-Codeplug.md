# Read codeplug from radio

Use option -r to read a codeplug from the radio to a binary file 'device.img'.
File format is compatible with md380tools.

A configuration in text format is also saved to file 'device.conf'.

```
$ dmrconfig -r
Connect to TYT MD-UV380.
Read device: ########################## done.
Last Programmed Date: 2018-08-31 23:23:13
CPS Software Version: V=0.01
Close device.
Write codeplug to file 'device.img'.
Print configuration to file 'device.conf'.
```
