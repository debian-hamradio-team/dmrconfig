# Extract configuration from codeplug

To convert a binary codeplug into a configuration script,
invoke dmrconfig without options, for example:

```
$ dmrconfig md-uv380-empty.rdt
Read codeplug from file 'md-uv380-empty.rdt'.
Radio: TYT MD-UV380

Analog  Name             Receive   Transmit Power Scan AS Sq TOT RO Admit  RxTone TxTone Width
    1   Channel1         400.000   +0       High  -    -  1  60  -  -      -      -      12.5

Zone    Name             Channels
   1a   Zone1            1
   1b   -                -

Scanlist Name             PCh1 PCh2 TxCh Channels
    1    ScanList1        -    -    Last 1

Contact Name             Type    ID       RxTone
    1   Contact1         Group   1        -

Grouplist Name             Contacts
    1     GroupList1       1

Message Text
    1   Hello

ID: 1234
Name: -
Intro Line 1: -
Intro Line 2: -
```

Input binary file can have IMG format (compatible with md380tools)
or RDT format (compatible with native CPS software).

When redirected to a file, the script will contain comments,
which describe the details of the configuration.
For examples, see [dmrconfig/examples](https://github.com/sergev/dmrconfig/tree/master/examples).
